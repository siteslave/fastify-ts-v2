export type TFileInfo = {
    file_id: string;
    user_id: string;
    file_name: string;
    file_type: string;
    file_path: string;
};
