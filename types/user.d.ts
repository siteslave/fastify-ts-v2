export type TUser = {
    user_id: string;
    username: string;
    password: string;
    fname: string;
    lname: string;
};
