FROM node:lts-alpine as build
WORKDIR /app
COPY ./package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM node:lts-alpine AS production
WORKDIR /app
RUN chown -R node:node /app
USER node
COPY --from=build --chown=node:node /app/dist .
COPY --from=build --chown=node:node /app/package.json .
COPY --from=build --chown=node:node /app/package-lock.json .
RUN npm ci --omit dev

EXPOSE 3000
CMD ["node", "./server.js"]