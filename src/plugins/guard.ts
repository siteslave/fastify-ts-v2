import fp from 'fastify-plugin';

import fastifyAuth from '@fastify/auth';

export default fp(async (fastify) => {
    fastify.register(fastifyAuth);

    fastify.decorate('authorize', async (request: any, reply: any): Promise<any> => {
        const allowedRoles = request.routeOptions.config.allowedRoles;
        const allowedScopes = request.routeOptions.config.allowedScopes;

        // Check if role is allowed
        if (allowedRoles && request.jwtDecoded.sub) {
            const userRoles = request.jwtDecoded.sub ? request.jwtDecoded.roles! : [];

            if (!userRoles) {
                return reply
                    .status(401)
                    .send({ ok: false, statusCode: 401, error: 'No roles defined' });
            }

            try {
                if (!allowedRoles.some((role: any) => userRoles.includes(role))) {
                    return reply
                        .status(401)
                        .send({ ok: false, statusCode: 401, error: 'Permission denied' });
                }
            } catch (error) {
                return reply
                    .status(500)
                    .send({ ok: false, statusCode: 500, error: 'Cannot read properties of roles' });
            }
        }

        // Check if scope is allowed
        if (allowedScopes && request.jwtDecoded.sub) {
            const userScopes = request.jwtDecoded.sub ? request.jwtDecoded.scopes! : [];

            if (!userScopes) {
                return reply
                    .status(401)
                    .send({ ok: false, statusCode: 401, error: 'No scopes defined' });
            }

            try {
                if (!allowedScopes.some((scope: any) => userScopes.includes(scope))) {
                    return reply
                        .status(401)
                        .send({ ok: false, statusCode: 401, error: 'Permission denied' });
                }
            } catch (error) {
                return reply.status(500).send({
                    ok: false,
                    statusCode: 500,
                    error: 'Cannot read properties of scopes',
                });
            }
        }
    });
});

declare module 'fastify' {
    type Authorize = (request: any, reply: any) => Promise<void>;
    interface FastifyContextConfig {
        allowedRoles?: string[];
        allowedScopes?: string[];
    }
    interface FastifyInstance {
        authorize: Authorize;
    }
}
