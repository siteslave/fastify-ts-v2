import { FastifyReply, FastifyRequest } from 'fastify';
import fp from 'fastify-plugin';
import { readFileSync } from 'node:fs';
import path from 'node:path';

import FastifyJwt, { JWT } from '@fastify/jwt';

export default fp(async (fastify) => {
    await fastify.register(FastifyJwt, {
        decoratorName: 'jwtDecoded',
        secret: {
            private: readFileSync(`${path.join(__dirname, '../../certs')}/rsa.key`, 'utf8'),
            public: readFileSync(`${path.join(__dirname, '../../certs')}/rsa.key.pub`, 'utf8'),
        },
        sign: {
            algorithm: 'RS256',
            iss: process.env.JWT_ISSUER || 'auth.my.dev',
            expiresIn: process.env.JWT_EXPIRES_IN,
        },
        verify: {
            allowedIss: process.env.JWT_ISSUER || 'auth.my.dev',
        },
        messages: {
            badRequestErrorMessage: 'Format is Authorization: Bearer [token]',
            noAuthorizationInHeaderMessage: 'Autorization header is missing!',
            authorizationTokenExpiredMessage: 'Authorization token expired',
            authorizationTokenInvalid: (err: any) => {
                return `Authorization token is invalid: ${err.message}`;
            },
        },
    });

    fastify.decorate('authenticate', async (request: FastifyRequest, reply: FastifyReply) => {
        await request.jwtVerify();
    });
});

declare module 'fastify' {
    export interface FastifyInstance {
        jwt: JWT;
        authenticate: any;
    }

    type Payload = {
        sub: string;
        hospcode?: string;
        roles?: string[];
        scopes?: string[];
    };

    export interface FastifyRequest {
        jwtVerify(): any;
        jwtDecoded: Payload;
    }
}
