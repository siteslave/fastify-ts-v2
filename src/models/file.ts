import { Knex } from 'knex';

import { TFileInfo } from '../../types/file';

export class FileModel {
    private db!: Knex;

    constructor(db: Knex) {
        this.setConnection(db);
    }

    /**
     * Set Database connection
     * @param db Knex
     */
    setConnection(db: Knex) {
        this.db = db;
    }

    /**
     * User login
     * @param username
     */
    save(fileName: string, filePath: string, fileType: string, userId: string): Promise<void> {
        return this.db('files').insert({
            file_name: fileName,
            file_path: filePath,
            file_type: fileType,
            user_id: userId,
        });
    }

    remove(fileId: string): Promise<void> {
        return this.db('files').where('file_id', fileId).del();
    }

    getInfo(fileId: string): Promise<TFileInfo> {
        return this.db('files').where('file_id', fileId).first();
    }
}
