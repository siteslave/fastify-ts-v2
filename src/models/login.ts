import { Knex } from 'knex';

import { TUser } from '../../types/user';

export class Login {
    private db!: Knex;

    constructor(db: Knex) {
        this.setConnection(db);
    }
    /**
     * Set Database connection
     * @param db Knex
     */
    private setConnection(db: Knex): void {
        this.db = db;
    }

    /**
     * User login
     * @param username
     */
    check(username: string): Promise<TUser> {
        return this.db('users')
            .where('username', username)
            .select('user_id', 'password', 'fname', 'lname')
            .first();
    }
}
