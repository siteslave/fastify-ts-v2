import { Knex } from 'knex';

import { TUser } from '../../types/user';

export class User {
    private db!: Knex;

    constructor(db: Knex) {
        this.setConnection(db);
    }

    /**
     * Set Database connection
     * @param db Knex
     */
    private setConnection(db: Knex): void {
        this.db = db;
    }

    /**
     * Register new user
     * @param db Knex
     * @returns
     */
    save(username: string, password: string, fname: string, lname: string): Promise<void> {
        return this.db('users').insert({
            username,
            password,
            fname,
            lname,
        });
    }
    /**
     * Update user
     * @param db Knex
     * @returns
     */
    update(userId: string, fname: string, lname: string): Promise<void> {
        return this.db('users').where('user_id', userId).update({
            fname,
            lname,
        });
    }

    /**
     * Remove user
     * @returns
     */
    remove(userId: string): Promise<void> {
        return this.db('users').where('user_id', userId).del();
    }

    /**
     * Get user list
     * @param limit
     * @param offset
     * @returns
     */
    list(limit: number, offset: number): Promise<TUser[]> {
        return this.db('users')
            .select('user_id', 'username', 'fname', 'lname')
            .orderBy('fname', 'asc')
            .limit(limit)
            .offset(offset);
    }

    /**
     * Get total number of users
     * @returns
     */
    async total(): Promise<number> {
        const result = await this.db('users').count('* as total').first();
        return Number(result?.total || '0');
    }
}
