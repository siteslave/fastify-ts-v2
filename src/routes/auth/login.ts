import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import S from 'fluent-json-schema';
import { Knex } from 'knex';

import { Login } from '../../models/login';
import { Hash } from '../../utils/hash';

export default async (fastify: FastifyInstance): Promise<void> => {
    const hash = new Hash();
    const db = fastify.db as Knex;

    const login = new Login(db);

    /**
     * User login
     * @param username
     * @param password
     */
    fastify.post<{
        Body: { username: string; password: string };
        Reply: { ok: boolean; token?: string; error?: string; statusCode?: number; reqId?: string };
    }>(
        '/login',
        {
            config: {
                rateLimit: {
                    max: 3,
                    timeWindow: '1 minute',
                },
            },
            schema: {
                body: S.object()
                    // Username min 4 and max 20
                    .prop('username', S.string().maxLength(20).minLength(4))
                    // Password min 4 and max 20
                    .prop('password', S.string().maxLength(20).minLength(4)),
            },
        },
        async function (request: FastifyRequest, reply: FastifyReply) {
            const { username, password } = request.body as { username: string; password: string };
            try {
                // Check username in database
                const userinfo = await login.check(username);

                // Username not found
                if (!userinfo) {
                    // Send response to client
                    return reply.status(401).send({
                        ok: false,
                        statusCode: 10400,
                        error: 'Username not found.',
                        reqId: request.id,
                    });
                }

                // Password not match
                if (!(await hash.comparePassword(password, userinfo.password))) {
                    return reply.status(401).send({
                        ok: false,
                        statusCode: 10401,
                        error: 'Password not match.',
                        reqId: request.id,
                    });
                }

                // Create jwt
                const token: string = fastify.jwt.sign({
                    sub: userinfo.user_id,
                    roles: ['admin'],
                    scopes: ['user:add', 'user:read'],
                });

                // Set logging
                request.log.warn({
                    message: 'Login success!',
                    reqId: request.id,
                    module: 'LOGIN',
                    data: {
                        user_id: userinfo.user_id,
                    },
                });

                // Response to client
                return reply.send({ ok: true, statusCode: 10200, token, reqId: request.id });
            } catch (error) {
                // Set logging
                request.log.error({
                    message: error,
                    reqId: request.id,
                    module: 'LOGIN',
                });

                // Send response to client
                return reply.status(500).send({
                    ok: false,
                    statusCode: 10500,
                    error: 'Internal server error.',
                    reqId: request.id,
                });
            }
        }
    );
};
