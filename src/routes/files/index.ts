import { FastifyInstance } from 'fastify';

import getFile from './getFile';
import remove from './remove';

export default async (fastify: FastifyInstance): Promise<void> => {
    // Verify JWT
    fastify.addHook('onRequest', (request) => request.jwtVerify());

    // Register routes
    fastify.register(getFile);
    fastify.register(remove);
};
