import { FastifyInstance } from 'fastify';

import upload from './upload';

const multipart = import('@fastify/multipart');

export default async (fastify: FastifyInstance): Promise<void> => {
    // Verify JWT
    fastify.addHook('onRequest', (request) => request.jwtVerify());

    // Register multipart plugin
    fastify.register(multipart, {
        // Can attach fields to body
        attachFieldsToBody: true,
        // File size limit
        limits: {
            fileSize: 1024 * 1024 * 10 /** 10M */,
        },
    });

    // Register routes
    fastify.register(upload);
};
