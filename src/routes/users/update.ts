import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import S from 'fluent-json-schema';
import { Knex } from 'knex';

import { User } from '../../models/user';

export default async (fastify: FastifyInstance): Promise<void> => {
    const db = fastify.db as Knex;
    const user = new User(db);

    /**
     * User registration
     * @param id User id
     * @param fname First name
     * @param lname Last name
     */
    fastify.put(
        '/:id/update',
        {
            schema: {
                body: S.object()
                    .prop('fname', S.string().maxLength(20).minLength(4))
                    .prop('lname', S.string().maxLength(20).minLength(4)),
                params: S.object().prop('id', S.string().format('uuid')),
            },
        },
        async function (request: FastifyRequest, reply: FastifyReply) {
            const { fname, lname } = request.body as {
                fname: string;
                lname: string;
            };

            const { id } = request.params as { id: string };

            try {
                // Update user
                await user.update(id, fname, lname);

                // Response to client
                return reply.status(200).send({ ok: true });
            } catch (error) {
                // Set logging
                request.log.error({
                    message: JSON.stringify(error),
                    reqId: request.id,
                    module: 'USER',
                });

                return reply.status(500).send({
                    ok: false,
                    error: 'Internal server error.',
                    reqId: request.id,
                    statusCode: 10500,
                });
            }
        }
    );
};
