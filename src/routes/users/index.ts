import { FastifyInstance } from 'fastify';

import list from './list';
import register from './register';
import remove from './remove';
import update from './update';

export default async (fastify: FastifyInstance): Promise<void> => {
    // Verify JWT
    fastify.addHook('onRequest', (request) => request.jwtVerify());

    // Register routes
    fastify.register(list);
    fastify.register(register);
    fastify.register(update);
    fastify.register(remove);
};
