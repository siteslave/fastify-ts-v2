import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import S from 'fluent-json-schema';
import { Knex } from 'knex';

import { TUser } from '../../../types/user';
import { User } from '../../models/user';

export default async (fastify: FastifyInstance): Promise<void> => {
    const db = fastify.db as Knex;
    const user = new User(db);

    fastify.get(
        '/',
        {
            schema: {
                querystring: S.object()
                    .prop('limit', S.number().maximum(100).default(10))
                    .prop('offset', S.number().default(0)),
            },
        },
        async function (request: FastifyRequest, reply: FastifyReply) {
            const { limit, offset } = request.query as { limit: number; offset: number };

            try {
                const users: TUser[] = await user.list(limit, offset);
                const total = await user.total();

                return reply.send({ ok: true, results: users, total, reqId: request.id });
            } catch (error) {
                // Set logging
                request.log.error({
                    message: JSON.stringify(error),
                    reqId: request.id,
                    module: 'USER',
                });
                return reply.status(500).send({ ok: false, error: 'Internal server error.' });
            }
        }
    );
};
