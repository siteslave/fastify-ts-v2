import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import S from 'fluent-json-schema';
import { Knex } from 'knex';

import { User } from '../../models/user';
import { Hash } from '../../utils/hash';

export default async (fastify: FastifyInstance): Promise<void> => {
    const hash = new Hash();

    const db = fastify.db as Knex;
    const user = new User(db);

    /**
     * User registration
     * @param username
     * @param password
     * @param fname
     * @param lname
     */
    fastify.post(
        '/',
        {
            schema: {
                body: S.object()
                    .prop('username', S.string().maxLength(20).minLength(4))
                    .prop('password', S.string().maxLength(20).minLength(4))
                    .prop('fname', S.string().maxLength(20).minLength(4))
                    .prop('lname', S.string().maxLength(20).minLength(4)),
            },
        },
        async function (request: FastifyRequest, reply: FastifyReply) {
            const { username, password, fname, lname } = request.body as {
                username: string;
                password: string;
                fname: string;
                lname: string;
            };

            try {
                // Hash password
                const hashed = await hash.hashPassword(password);
                // Create new user
                await user.save(username, hashed, fname, lname);
                // Response to client
                return reply.status(201).send({ ok: true, reqId: request.id });
            } catch (error) {
                // Set logging
                request.log.error({
                    message: JSON.stringify(error),
                    reqId: request.id,
                    module: 'USER',
                });

                return reply.status(500).send({
                    ok: false,
                    statusCode: 500,
                    error: 'Internal server error.',
                    reqId: request.id,
                });
            }
        }
    );
};
