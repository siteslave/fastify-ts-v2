import { FastifyInstance } from 'fastify';

import role from './role';
import scope from './scope';

export default async (fastify: FastifyInstance): Promise<void> => {
    // Verify JWT
    fastify.addHook('onRequest', (request) => request.jwtVerify());

    // Register routes
    fastify.register(role);
    fastify.register(scope);
};
