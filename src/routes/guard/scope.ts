import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';

export default async (fastify: FastifyInstance): Promise<void> => {
    fastify.get(
        '/scopes',
        {
            config: {
                allowedRoles: ['admin'],
                allowedScopes: ['user:read'],
            },
            preHandler: [fastify.auth([fastify.authorize])],
        },
        async function (request: FastifyRequest, reply: FastifyReply) {
            reply.send({ ok: true, message: 'Ok!, you have permission' });
        }
    );
};
