import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';

export default async (fastify: FastifyInstance): Promise<void> => {
    fastify.get(
        '/roles',
        {
            config: {
                allowedRoles: ['admin'],
            },
            preHandler: [fastify.auth([fastify.authorize])],
        },
        async function (request: FastifyRequest, reply: FastifyReply) {
            reply.send({ ok: true, message: 'Welcome admin!' });
        }
    );
};
